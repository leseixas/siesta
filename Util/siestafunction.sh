#  SIESTA shell functions
#
#    Leandro Seixas <leandro.seixas@mackenzie.br>
#    MackGraphe - Graphene and Nanomaterials Research Center
#    Mackenzie Presbyterian University
#

function siesta_forces() {
  grep constrained job.out | awk '{print $2}'
}

function siesta_clean() {
  rm -f H_DMGEN *.bib DM* *.ion* INPUT_TMP.* fdf* Rho.grid.nc FORCE_STRESS CLOCK OCCS BASIS_* NON_TRIMMED_* H_MIXED PARALLEL_DIST MESSAGES 0_NORMAL_EXIT
}

function siesta_energy() {
  grep "Total =" job.out | cut -d = -f 2
}

function run_siesta() {
  NPROC=$(nproc)
  OMP_NUM_THREADS=$NPROC ~/siesta/Obj/siesta < input.fdf > job.out &
}
