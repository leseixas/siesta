#!/bin/bash
#  Build SIESTA on Ubuntu 18.04 with MPI parallelization 
#
#    Leandro Seixas <leandro.seixas@mackenzie.br>
#    MackGraphe - Graphene and Nanomaterials Research Center
#    School of Engineering
#    Mackenzie Presbyterian University
#

# installing dependencies

sudo apt-get update
sudo apt-get install make gfortran libopenblas-dev libfftw3-dev openmpi-common openmpi-bin \
                     libopenmpi-dev libblacs-mpi-dev libscalapack-openmpi-dev libnetcdf-dev \
                     netcdf-bin libnetcdff-dev libblas-dev liblapack-dev

# making
cd Obj
bash ../Src/obj_setup.sh
cp openmpi.make arch.make
make -j4
cd ..

