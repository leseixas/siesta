#!/bin/bash
#  Ubuntu 18.04 dependencies 
#
#    Leandro Seixas <leandro.seixas@mackenzie.br>
#    MackGraphe - Graphene and Nanomaterials Research Center
#    School of Engineering
#    Mackenzie Presbyterian University
#

sudo apt-get update
sudo apt-get install make gfortran libopenblas-dev libomp-dev 
