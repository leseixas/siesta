#!/bin/bash
#  SIESTA shell functions
#
#    Leandro Seixas <leandro.seixas@mackenzie.br>
#    MackGraphe - Graphene and Nanomaterials Research Center
#    School of Engineering
#    Mackenzie Presbyterian University
#

path=$(pwd)

sed "s|VAR_PATH|$path|g" template_functions >> ~/.bashrc 

source ~/.bashrc
