#!/bin/bash
#  Build SIESTA on Ubuntu 18.04 with OpenMP parallelization 
#
#    Leandro Seixas <leandro.seixas@mackenzie.br>
#    MackGraphe - Graphene and Nanomaterials Research Center
#    School of Engineering
#    Mackenzie Presbyterian University
#

# installing dependencies
sudo apt-get update
sudo apt-get install make gfortran libopenblas-dev libomp-dev

# making
cd Obj
bash ../Src/obj_setup.sh
cp omp.make arch.make
make -j4
cd ..

